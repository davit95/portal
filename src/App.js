import React from 'react';
import IdleTimer from 'react-idle-timer';
import 'antd/dist/antd.css';
import AppRouter from '../src/components/App/AppRouter';
import { message } from 'antd';
import { client } from './Client';
import http from './http';

class App extends React.Component {
  
  logout = async () => {
    try {
      Object.assign(http.defaults,
        {
          headers:
          {
            'Authorization': `Bearer ${localStorage.token}`
          }
        }
      )
      const response = await http.post('logout');
      if (response.data.status === 'success') {
        client.logout();
        this.redirect();
        message.success(response.data.message);
      } else {
        message.error(response.data.message);
      }
      window.location.reload(true);
    } catch (error) {
      message.error(error.message);
      window.location.href = '/';
    }
  }
 
  handleOnIdle = () => {
    if (localStorage.getItem('token')) {
      this.logout();
    } 
  }

  render () {
    return (
      <div>
        <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          timeout={1000 * 60}
          onActive={this.handleOnActive}
          onIdle={this.handleOnIdle}
          onAction={this.handleOnAction}
          debounce={250}
        />
        <AppRouter />
      </div>
    )
  }
}


export default App;
